# This is an H1
## This is an H2
###### This is an H6


Indent every line of the block by at least 4 spaces.

This is a normal paragraph:

    This is a code block.
    With multiple lines.

Alternatively, you can use 3 backtick quote marks before and after the block, like this:

```
This is a code block
```

To add syntax highlighting to a code block, add the name of the language immediately
after the backticks: 

```javascript
var oldUnload = window.onbeforeunload;
window.onbeforeunload = function() {
    saveCoverage();
    if (oldUnload) {
        return oldUnload.apply(this, arguments);
    }
};
```

...
<--Collected image definitions-->
[MockupA]: https://cdn.vox-cdn.com/thumbor/CcznmYG3iVJ6KOSklGohcKbvQ_w=/1400x1400/filters:format(jpeg)/cdn.vox-cdn.com/uploads/chorus_asset/file/10455373/mdoying_180118_2249_0338stills.jpg "Screenshot of Feature A mockup" 
...
<!--Using an image reference-->
![Mockup for feature A][MockupA]
...

| Day     | Meal    | Price |
| --------|---------|-------|
| Monday  | pasta   | $6    |
| Tuesday | chicken | $8    |